package com;

import com.google.api.client.util.Charsets;
import com.google.common.io.CharStreams;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.FileSystems;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.options.*;
import org.apache.beam.sdk.transforms.SerializableFunction;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class BQToBQ {

    public interface Options extends PipelineOptions {

        @Validation.Required
        @Description("Output table to write to")
        ValueProvider<String> getOutputTable();
        void setOutputTable(ValueProvider<String> value);

        @Validation.Required
        @Description("Output table to write to")
        ValueProvider<String> getQueryFile();
        void setQueryFile(ValueProvider<String> value);

        @Validation.Required
        @Description("Output table to write to")
        ValueProvider<String> getOutputTableDescriptor();
        void setOutputTableDescriptor(ValueProvider<String> value);

        @Validation.Required
        @Description("Temporary directory for BigQuery loading process")
        ValueProvider<String> getBigQueryLoadingTemporaryDirectory();
        void setBigQueryLoadingTemporaryDirectory(ValueProvider<String> directory);

    }
    private static String readFile(String inputPath){
        try {
            ReadableByteChannel chan = FileSystems.open(FileSystems.matchNewResource(inputPath, false));
            InputStream stream = Channels.newInputStream(chan);
            String content = CharStreams.toString(new InputStreamReader(stream, Charsets.UTF_8));
            return content;
        } catch (IOException e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }

    public static void main(String[] args){
        BQToBQ.Options options = PipelineOptionsFactory.fromArgs(args).as(BQToBQ.Options.class);
        FileSystems.setDefaultPipelineOptions(options);

        //Construct the pipeline
        Pipeline p = Pipeline.create(options);
        p.apply("Read Table", BigQueryIO.readTableRows().usingStandardSql().withoutValidation().fromQuery(
                ValueProvider.NestedValueProvider.of(
                        options.getQueryFile(),
                        new SerializableFunction<String, String>() {
                            @Override
                            public String apply(String queryFile) {
                                return readFile(queryFile);
                            }
                        })))
                .apply("UpLoads to BigQuery", BigQueryIO.writeTableRows()
                        .to(options.getOutputTable())
                        .withCustomGcsTempLocation(options.getBigQueryLoadingTemporaryDirectory())
                        .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_IF_NEEDED)
                        .withJsonSchema(
                                ValueProvider.NestedValueProvider.of(
                                        options.getOutputTableDescriptor(),
                                        new SerializableFunction<String, String>() {
                                            @Override
                                            public String apply(String jsonPath) {
                                                return readFile(jsonPath);
                                            }
                                        }))
                        .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                );
        p.run();
    }
}
