# DATAFLOW BIGQUERY TO BIGQUERY CUSTOM TEMPLATE

This Dataflow template allows you to run an SQL query on BigQuery and to load the result in a BigQuery table.

## DEPLOYMENT

-   Clone the project
-   Move to the root of the project: 
        cd DataflowBQToBQ
-   Execute the maven command:

mvn compile exec:java \
     -Dexec.mainClass=com.BQToBQ \
     -Dexec.args="--runner=DataflowRunner \
                  --project={YOUR_PROJECT_ID} \
                  --stagingLocation=gs://{YOUR_BUCKET_NAME}/staging \
                  --templateLocation=gs://{YOUR_BUCKET_NAME}/templates/BQToBQ"

*Replace {YOUR_PROJECT_ID} and {YOUR_BUCKET_NAME}*

-   Copy the template metadata file to your template repository on GCS:
        gsutil cp src/main/resources/BQToBQ_metadata gs://{YOUR_BUCKET_NAME}/templates/


## EXECUTION

-   On the GCP console go to Dataflow --> Create job from template
-   Fill the job name and Regional endpoint fields
-   In the template selection field: select "Custom  Template"
-   In the template GCS path field: Click "Browse" --> {YOUR_BUCKET_NAME} --> templates --> BQToBQ
-   Fill all the required parameters
-   Click "Run job"

*If the specified output table doesn't exist it will be created, if it already exists results of the query wil be appended to it.*

## RUNTIME PARAMETERS

-   **queryFile**: GCS path of the file containing the query, ex: gs://MyBucket/query.sql , 
        exemple of file content : SELECT * FROM {DATASET}.{TABLE}
-   **outputTable**: BigQuery table where the output will be loaded, ex: {PROJECT_ID}:{DATASET}.{TABLE}
-   **outputTableDescriptor**: Path of the JSON descriptor file for the output table structure, ex: gs://{YOUR_BUCKET_NAME}/desc.json , 
        exemple of file content : 
            {
              "fields": [
                {
                  "name": "name",
                  "type": "STRING",
                  "mode": "REQUIRED"
                },
                {
                  "name": "age",
                  "type": "INT64",
                  "mode": "REQUIRED"
                },
                {
                  "name": "location",
                  "type": "STRING"
                }
              ]
            }
-   **bigQueryLoadingTemporaryDirectory**: Temporary directory for BigQuery loading process, ex: gs://my-bucket/my-files/temp_dir
-   **tempLocation**: Temporary directory for Dataflow job, ex: gs://my-bucket/my-files/temp_dir
